<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>  
  
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .' hLine"';  } ?>>
	<? if( ($id + 1) % 2 =='0' ){echo '<div class="hLine">';} ?>  
		<?php print $row; ?>
	<? if( ($id + 1) % 2 =='0' ){echo '</div>';} ?>
  </div>
	<? if( ($id + 1) % 2 =='0' ){echo '<div style="clear:both"></div>';} ?>
<?php endforeach; ?>
