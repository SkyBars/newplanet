<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<script>
	
	jQuery(document).ready(function(){		
		jQuery('input.count').each(function(){
			var nId = jQuery(this).parents('tr.productListing').find('td.nodeId > input').attr('value'); 						
			jQuery(this).attr('id', nId);			
		})	
	})

	
	function add_to_basket(){
		var arr = [];
		jQuery('input.count').each(function(){
			if(!(jQuery(this).val() == ''))
			{
				var nid = parseInt(jQuery(this).attr('id')); 
				var qty = parseInt(jQuery(this).val());				
				if(isNaN(qty)){
					alert('Количество товара дожно быть целочисленным значением');							
				}else{					
					arr[nid] = qty;					
				}				
			}		
		})
		
		jQuery.post(Drupal.settings.basePath + 'ajax-add-to-cart', {array:arr}, function(response) {		
			if (!response.status) {return alert(response.data);} 					
			jQuery('#block-block-2 > div.content').html(response.data.content);
			alert('Товары добавлены');
		}); 
		
		
		return true;
	}	
	
</script>
<div class="tie tie-margin1" style="margin-right: -20px; padding-bottom: 40px;">
<div class="tie-indent">
<div class="buttonRow forward"><input type="image" src="/sites/all/themes/newplanet/images/button_add_selected.gif" onClick="return add_to_basket();"></div>

<table class="tabTable">  
  <tbody>
	<tr class="productListing-rowheading">
		<th class="productListing-heading" align="center" scope="col" id="listCell0-0">Изображение</th>
		<th class="productListing-heading" align="center" scope="col" id="listCell0-1">Название</th>
		<th class="productListing-heading" align="center" scope="col" id="listCell0-2">Цена</th>
	</tr>
    <?php foreach ($rows as $row_count => $row): ?>
      <tr class="productListing">
        
		<?php foreach ($row as $field => $content): ?>
        
			
			<?php if($field_classes[$field][$row_count] !='listingDescription'): ?>
				<td  				
					<?if($field_classes[$field][$row_count] == 'node-id'){print 'style="display:none;" class="nodeId"';}else{print 'class="productListing-data"';}?>
					<?if($field_classes[$field][$row_count] == 'price'){print '"nowrap" style="text-align:center"';} ?>
				> 
			<?php endif;?>
        
					
					<?php 					
					switch($field_classes[$field][$row_count])
						{
							case 'itemTitle':
								print '<h3 class="'.$field_classes[$field][$row_count].'">'.$content.'</h3>';
							break;						
							case 'price':
								print '<span class="'.$field_classes[$field][$row_count].'">'.$content.'</span><br/><br/><input placeholder="Введите количество" type="text" class="count" name="count" value="" id="id">';
							break;						
							case 'node-id':
								print '<input type="hidden" value="'.$content.'" class="nodeId" />';
							break;													
							case 'listingDescription':
								'<div class="'.$field_classes[$field][$row_count].'">'.$content.'</h3>';
							break;
							default:
								print $content;
							break;				
						}					
					?>

			<?php if($field_classes[$field][$row_count] !='itemTitle'): ?>
				</td> 
			<?php endif;?>

        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>  
</table>
<div class="buttonRow forward"><input type="image" src="/sites/all/themes/newplanet/images/button_add_selected.gif" onClick="return add_to_basket();"></div>
</div>
</div>