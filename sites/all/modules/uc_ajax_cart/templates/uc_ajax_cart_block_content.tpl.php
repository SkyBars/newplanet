<?php
/**
 * @file
 *
 * Theme file for non empty cart.
 */   

//print_r($items);

 ?>


 
<script>

jQuery('input.count').change(function(){
	var inputLink = jQuery(this);
	var qty = jQuery(this).val();
	var cart_item_id = jQuery(this).attr('cart_item_id');
	var sku = jQuery(this).attr('sku');
	var cart_id = jQuery(this).attr('cart_id');
	
	jQuery.post(Drupal.settings.basePath + 'ajax-update-count', {qty:qty, cart_item_id:cart_item_id, sku:sku, cart_id:cart_id}, function(response) {		
		
		if(!response.status)
		{
			alert(response.message);						
			jQuery(inputLink).val(response.stock);
			jQuery('div#ajaxCartUpdate').parents('div.content:first').html(response.data.content);
		}		
		else
		{			
			jQuery('div#ajaxCartUpdate').parents('div.content:first').html(response.data.content);
			alert(response.message);
		}		
	});
})

function delete_item(cart_item_id, nid){
	if(confirm('Удалить товар?')){
		jQuery.post(Drupal.settings.basePath + 'ajax-delete-item', {nid:nid, cart_item_id:cart_item_id}, function(response) {		
			
			if(!response.status)
			{
				alert(response.data);
				console.log(response);
			}		
			else
			{			
				jQuery('div#ajaxCartUpdate').parents('div.content:first').html(response.data.content);
				alert(response.message);
				console.log(response);
			}		
		});
	}
}

</script>


<div class="tie text2">
<div class="tie-indent" id="content-after-upd">
<div id="cart-block-contents-ajax">
  <table id="cartContentsDisplay">
    <thead>
      <tr class="tableHeading">
        <th scope="col" id="scQuantityHeading">Количество</th>
		<th scope="col" id="scUpdateQuantity">Наименование товара</th>
		<th scope="col" id="scProductsHeading">Стоимость<br/>за</br>еденицу</th>
		<th scope="col" id="scUnitHeading">Общая</br>стоимость</th>		
		<th scope="col" id="scRemoveHeading">Удалить</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ( $items as $key => $item ):?>
      <tr class="cartQuantity">
        <td class="cart-block-item-qty">
          <?php print '<input type="text" class="count" id="edit-items-'.$key.'-qty" name="items['.$key.'][qty]" value="'.$item['item']->qty.'" sku="'.$item['item']->title.'" cart_item_id="'.$item['item']->cart_item_id.'" cart_id="'.$item['item']->cart_id.'" size="5" maxlength="6" class="form-text required ajax-cart-processed" x-webkit-speech="">' ?>
        </td>
    
		<td class="cartQuantityUpdate buttonRow">
          <span id="cartProdTitle"><?php print $item['title'];?><span class="alert bold"></span></span>
		   <?php		   
			$var = array('item' => $item['image']['#item'],'image_style' => 'mini');
			$output = theme_image_formatter($var);							
		  ?>
		  <span id="cartImage" class="back"><?php print $output; ?></span>
        </td>
		
		<td scope="col" class="cartUnitDisplay price" style="text-align:center!important;">
			<?php print substr($item['item']->sell_price, 0, 6).' Р'; ?>
		</td>		
        <td class="cartTotalDisplay" style="font-weight:bold!important;; text-align:center!important;">
			<?php print $item['total'].' Р'; ?> 
		</td>
		<td><?php print '<input style="border:none" type="image" src="/sites/all/themes/newplanet/images/small_delete.gif" onclick="return delete_item('.$item['item']->cart_item_id.','.$item['nid'].');">' ?></td>
      </tr>      
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
</div>
</div>

<div id="cartSubTotal">
<?php print 'Итого:  ';?> 
<span class="price">
	<?php '  '.print $total.' Р' ;?>
</span>
</div>
<div class="shcart_btn">
	<div class="btn1">
		<a href="/cart/checkout"><img src="/sites/all/themes/newplanet/images/button_checkout.gif" alt="Оформить заказ" title="Оформить заказ" width="164" height="30"></a>
	</div> 
</div>