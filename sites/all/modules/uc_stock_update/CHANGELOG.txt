$Id: CHANGELOG.txt 7 2012-04-27 12:48:49Z david $

uc_stock_update 7.x-2.1.0-beta1
===============================

Rewritten for Drupal 7.x / Ubercart 3. First 7.x-2.1.x beta version for testing.

Designed to be extensible. Extension modules can define a hook to
provide import and export of additional fields and tables.


uc_stock_update 6.x-2.0.2
=========================

Bugfixes:

- by david@davidhoulder.com. Fixed spurious "no match" error when new
  values match old values. See
  http://www.ubercart.org/comment/64614/possible_explanation_couldnt_find_existing_entry

uc_stock_update 6.x-2.0.1
=========================

First public version of Stock & Price CSV Updater 2
