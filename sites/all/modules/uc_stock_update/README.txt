;$Id: README.txt 8 2012-04-27 13:10:09Z david $

INSTALLATION
------------

 1) Copy the uc_stock_update directory into the appropriate modules directory in
    your Drupal/Übercart installation (recommended: sites/all/modules/).

 2) Log in as the site administrator.

 3) Choose "Modules" (admin/modules) from the main toolbar and enable
    the "Stock and Price Updater" module.

 3) Follow the "Permissions" link next to the module name.  Assign
    "Import stock and price data from CSV files" permission to any
    roles that require it.

USAGE
-----

Choose "Store" from the main toolbar (admin/store).
Scroll down to the PRODUCTS section.
Follow the link labelled "Update stock and price data from CSV files".
Follow the instructions on that page (admin/store/products/update-stock-csv).


EXTENDING
---------

Write a module that implements hook_uc_stock_update_handlers(). See
./uc_stock_update.module
./UcStockUpdate.inc

