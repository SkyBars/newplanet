<?php
// $Id: UcStockUpdate.inc 7 2012-04-27 12:48:49Z david $  
/**
 * @file
 *
 * Defines classes to handle import and export of CSV files.
 *
 * The classes defined below handle import and export of product
 * prices, stock levels and product option prices. 
 *
 * To enable import and export of other fields or tables, write a
 * module that...
 * - defines one or more classes that extend UcStockUpdateBase
 * - defines hook_uc_stock_update_handlers()
 * The module's .info file must include 
 *  files[] = ../uc_stock_update/UcStockUpdate.inc
 * in order to autoload UcStockUpdateBase
 */


/**
 * Abstract base class to handle import and export of CSV files.
 */
abstract class UcStockUpdateBase {
  /**
   * Used when exporting data to a CSV file.
   * args: URL query args from $_GET. 
   * @return: a select query containing all the fields to write to the CSV file.
   */
  abstract public function build_select($args);

  /**
   * Used to find a row to update on import.
   * $query: either a db_update() or db_select() query
   * $keys: key values from the CSV file in the form array(db_field_name => value, ...)
   * @return: The result of $query->condition(....)
   */
  abstract protected function update_condition($query, $keys);

  /**
   * Construct the update query.  Override this if necessary in child classes.
   * $keys: key values from the CSV file in the form array(db_field_name => value, ...)
   * @return: An update query ready to accept a ->fields() call.
   */
  protected function build_update($keys) {
    return $this->update_condition(db_update($this->update_table), $keys);
  }

  /**
   * Count rows matching key values. This is called after an update if
   * the reported number of affected rows is zero. If the new values
   * match the existing values in an update, some databases consider
   * the row to be unaffected, so this is the only way I know of
   * disambiguating this case from the case where no row matched the
   * keys.
   * @return: row count
   */
  protected function check_update($keys) {
    // Override if necessary in child class
    return  $this->update_condition(db_select($this->update_table), $keys)
      ->countQuery()->execute()->fetchField();
  }

  /**
   * Update a row in the database from a  row in the CSV file.
   * $keys: database keys in the form array(db_column_name => value, ...)
   * $values: new values for the row in the form array(db_column_name => value, ...)
   */
  public function import_row($keys, $values) {
    $query = $this->build_update($keys)->fields($values);
    $num_updated = $query->execute();
    if ($num_updated == 0) {
      // May be due to failure to match the condition, or new values == old values.
      // The only way to tell is to try and find a match
      $num_updated = $this->check_update($keys);
    }
    return $num_updated;
  }


  /**
   * Add form elements to handle this type of CSV file.
   * $form (in/out): Drupal form array to modify
   * $csv_name: CSV filename
   * #return: void
   */
  public function add_form_elements(&$form, $csv_name) {
    $ta=array('@label' => t($this->label));

    $form['csv_name_'.$this->id] =  array('#type' => 'value',
					  '#value' => $csv_name);

    $form['csv_export']['export_'.$this->id] = 
      array('#markup' => l(t('Export @label to CSV file.', $ta),
			  uc_stock_update_URL_ROOT . '/getcsv/'. $csv_name) .
	    '<br />');

    $description=
      t('The file must contain the columns').
      ' <strong>'. implode('</strong>, <strong>',
			   array_intersect_key($this->csv_headings,
					       $this->key_columns)).
      '</strong> '.
      (count($this->value_columns) >1? t('and one or more of'): t('and')).
      ' <strong>'. implode('</strong>, <strong>',
			   array_intersect_key($this->csv_headings,
					       $this->value_columns)).
      '</strong>';
    $form['source']['upload_'.$this->id] = array(
			     '#type' => 'file',
			     '#title' => t('Import @label CSV file', $ta),
			     '#description' => $description);
    $form['source'][$this->id] = array('#type' => 'submit',
				      '#value' => t('Update @label', $ta));
  }


  /**
   * Import callback for CSV file upload.
   */
  public function handle_submit(&$form_state) {
    $values = $form_state['values'];
    if ($values['delimiter'] == uc_stock_update_SEMICOLON) {
      $delimiter = ';';
    }
    else {
      $delimiter = ',';
    }

    $file=$form_state['saved_file'];
    $batch = array(
		   'operations' => array(),
		   'finished' => 'uc_stock_update_import_finished',
		   'title' => t('Importing data from CSV file'),
		   'init_message' => t('Starting import...'),
		   'progress_message' => t('Imported @percentage%.'),
		   'error_message' => t('An error occurred during the import.'),
		   'operations' => array(array('uc_stock_update_import_chunk',
					       array(get_class($this),
						     $delimiter,
						     $file->destination))));
    batch_set($batch);
  }


  /**
   * Export callback for CSV file download
   */
  public function export($csv_filename, $args=NULL) {
    $query = $this->build_select($args);
    $result_keys = array(); // to hold keys returned by fetchAssoc() in column order
    foreach ($this->select_fields as $table_fields) {
      foreach ($table_fields[1] as $f) {
	$alias = $query->addField($table_fields[0], $f);
	$result_keys[] = $alias;
      }
    }
    $result = $query->execute();
    
    $csv_data = implode(',', $this->csv_headings) ."\r\n";
    
    while ($r = $result->fetchAssoc()) {
      $row=array();
      foreach ($result_keys as $k) {
	$row[] = '"'. str_replace('"', '""', $r[$k]) .'"';
      }
      $csv_data .= implode(',', $row) ."\r\n";
    }
    
    if (ob_get_level()) {
      ob_end_clean();
    }

    drupal_add_http_header('Expires', '0');
    drupal_add_http_header('Cache-Control', 'must-revalidate');
    drupal_add_http_header('Content-Transfer-Encoding', 'binary');
    drupal_add_http_header('Content-Length', strlen($csv_data));
    drupal_add_http_header('Content-Disposition', 'attachment; filename="'. $csv_filename .'"');
    drupal_add_http_header('Content-Type', 'text/csv; charset=utf-8');
    
    /* Workaround for IE bug http://support.microsoft.com/kb/316431/ */
    
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != '' && $_SERVER['HTTPS'] != 'off') {
      drupal_add_http_header('Pragma', 'private');
      drupal_add_http_header('Cache-Control', 'private');
    } else {
      drupal_add_http_header('Pragma', 'no-cache');
      drupal_add_http_header('Cache-Control', 'no-cache');
    }
    drupal_send_headers();
    
    print $csv_data;
    drupal_exit();
  }
}



/**
 * Defines handling of product-prices import and export.
 */
class UcStockUpdateProducts extends UcStockUpdateBase {
  public $id = 'product_prices'; // For use in Drupal form element names
  public $label = 'product prices'; // Human-readable label (passed to t())
  // Column headings for CSV file
  public $csv_headings = array("PRODUCT_ID", "SKU", "name", "Cost", "List_price", "Sell_price");
  // Database columns corresponding to ->csv_headings
  public $select_fields = array(array('n', array('nid')),
				array('p', array('model')),
				array('n', array('title')),
				array('p', array('cost', 'list_price','sell_price')));

  // Table to update on import
  public $update_table = 'uc_products';

  // Which columns provide key values for the update, in the form
  // csv_headings_index => key_name_used_in_update_condition
  public $key_columns =  array(0 => 'nid',
			       1 => 'sku');

  // Which columns provide new values, in the form csv_column_index =>
  // array(db_field_name, filter_function)
  public $value_columns =  array(3 => array('cost', 'uc_stock_update_check_price'),
				 4 => array('list_price',  'uc_stock_update_check_price'),
				 5 => array('sell_price',  'uc_stock_update_check_price'));


  public function build_select($args /* not used */) {
    $query =  db_select('node', 'n');
    $query->join('uc_products', 'p', 'n.vid = p.vid');
    return $query->orderBy('p.model');
  }

  protected function update_condition($query, $keys) {
    // This builds  a sub-select like this:
    //  "model = :sku AND vid IN (SELECT vid from {node} WHERE nid = :nid)",
    // FIXME maybe just use 2 explicit queries instead?
    $subquery = db_select('node', 'n')->fields('n', array('vid'))->condition('n.nid', $keys['nid']);
    return $query->condition('model', $keys['sku'])->condition('vid', $subquery, 'IN');
  }
}

/**
 * Defines handling of stock-levels import and export.
 */
class UcStockUpdateStockLevel extends UcStockUpdateBase {
  public $id = 'stock_levels';
  public $label = 'stock levels';
  public $csv_headings =  array("PRODUCT_ID", "SKU", "name",
				"Active", "Stock_level", "Threshold");
  public $select_fields =  array(array('n', array('nid')),
				 array('s',array('sku')),
				 array('n', array('title')),
				 array('s', array('active', 'stock', 'threshold')));
  public $update_table = 'uc_product_stock';
  public $key_columns =  array(0 => 'nid',
			       1 => 'sku');
  public $value_columns =  array(3 => array('active', 'uc_stock_update_check_bool'),
				 4 => array('stock',  'uc_stock_update_check_stock_val'),
				 5 => array('threshold', 'uc_stock_update_check_stock_val'));

  public function build_select($args) {
    $query =  db_select('uc_product_stock', 's');
    $query->join('node', 'n', 'n.nid = s.nid');
    return $query->orderBy('s.sku');
  }
    
  protected function update_condition($query, $keys) {
    return $query->condition('sku', $keys['sku'])->condition('nid', $keys['nid']);
  }
}

/**
 * Defines handling of product-options import and export.
 */
class  UcStockUpdateProductOptions extends UcStockUpdateBase {
  public $id = 'product_options';
  public $label = 'product option prices';
  public $csv_headings = array("PRODUCT_ID",  "OPTION_ID",  "sku",
			       "option_name", "Cost", "Price");
  public $select_fields = array(array('n', array('nid')),
				array('o', array('oid')),
				array('p', array('model')),
				array('a', array('name')),
				array('o', array('cost', 'price')));
  public $update_table = 'uc_product_options';
  public $key_columns =  array(0 => 'nid',
			       1 => 'oid');
  public $value_columns =  array(4 => array('cost', 'uc_stock_update_check_price'),
				 5 => array('price',  'uc_stock_update_check_price'));
  
  public function build_select($args) {
    $query =  db_select('node', 'n');
    $query->join('uc_product_options', 'o', 'n.nid = o.nid');
    $query->join('uc_products', 'p', 'n.vid = p.vid');
    $query->join('uc_attribute_options', 'a', 'a.oid = o.oid');
    return $query->orderBy('p.model');
  }

  protected function update_condition($query, $keys) {
    return $query->condition('nid', $keys['nid'])->condition('oid', $keys['oid']);
  }
}
